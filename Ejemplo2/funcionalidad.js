let btn = document.getElementById("btn");

let resultado = document.getElementById("resultado");

let inputUno = document.getElementById("input-uno");
let inputDos = document.getElementById("input-dos");

btn.addEventListener("click", function(){

    let n1 = inputUno.value;
    let n2 = inputDos.value;

    resultado.innerHTML = suma(n1,n2);

});

function suma(n1, n2){
    return parseInt(n1) + parseInt(n2);
}